<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Article extends Model
{
    //

    use Searchable;

    protected $fillable = [

        'user_id',
        'title',
        'body',
        
    ];



    public function user(){

        return $this->belongsTo('App\User');
}


    public function searchableAs()
    {
        return 'articles_index';
    }


}
