<?php

namespace App\Events;

use App\Mail\Pdfemail;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MailSent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

   // public $pdfemail;


    /**
     * Create a new event instance.
     *
     * @param Pdfemail $pdfemail
     */
    public function __construct()//Pdfemail $pdfemail)
    {
        //
      //  $this->pdfemail = $pdfemail;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('mailSentEvent');
    }
}
