<?php

namespace App\Jobs;

use App\Events\HelloPusherEvent;
use App\Mail\Pdfemail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class ProcessPodcast implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */




    public function __construct()
    {
        //


    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //

        sleep(2);
        event(new HelloPusherEvent('sent successfuly'));
        Mail::to('vytautas.sabutis@gmail.com')->send(new Pdfemail());


    }
}
