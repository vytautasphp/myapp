<?php

namespace App\Http\Controllers;


use App\Article;
use App\Events\eventTrigger;
use App\Events\HelloPusherEvent;
use App\Jobs\ProcessPodcast;
use App\Mail\Pdfemail;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Requests\StoreArticle;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;



class ArticleController extends Controller


{

    public function ship(Request $request)
    {


        // Ship order...

       //Mail::to('vytautas.sabutis@gmail.com')->send(new pdfemail());
        //ProcessPodcast::dispatch();
            //->delay(now()->addSeconds(5));
      //  return "sent";

//        Mail::send('msg', [], function ($message) {
//            $message->to('vytautas.sabutis@gmail.com');
//            $message->from('sasdas@sdas.com');
//            $message->subject('Barborr komercinis pasiūlymas');
//            //$message->attachData($pdf->output(), 'pasiulymas.pdf');
//
//
//        });

        ProcessPodcast::dispatch()
        ->delay(now()->addSeconds(5));



        return "sent";

    }

    //
    public function index()
    {
        return Article::all();
    }

    public function googlecode(Request $request){

        $http = new Client;

        $response = $http->post('https://accounts.google.com/o/oauth2/v2/auth?', [

            'form_params' => [
                'scope' => 'https://mail.google.com/',
                'access_type' => 'offline',
                'include_granted_scopes' => 'true',
                'state' => 'state_parameter_passthrough_value',
                'redirect_uri' => 'https://accounts.google.com/o/oauth2/v2/auth?',
                'response_type' => 'code',
                'client_id' => '183346254246-16qlo8hpj9c4epdrbe7gblsvnr7sjcbe.apps.googleusercontent.com',
                
            ],
        ]);
        //$url = url()->full();

        return  $response;

     

        //dd($request);
    }


    public function email(User $id){


                // Validate the value...
                $token = Storage::get('token.txt');

                $client = new Client([
                
                    'headers' => ['Authorization' =>'Bearer '. $token],
                
                  ]);

                $response = $client->request('GET', 'https://www.googleapis.com/gmail/v1/users/vytautas.sabutis%40gmail.com/messages/161fa5fb2dddd42c');

                        //$data = $response->getBody();
                        //$data = json_decode($data);
                $data = json_decode((string) $response->getBody(), true);

               // return $data['payload']['headers'][5]['value'];
                //$mailpdf = "laisko ID: " . $data['id'] . " Tekstas: " . $data['snippet'];

          //Barryvdh/dompdf

                //$html = View::make('welcome')->render();

                $pdf = App::make('dompdf.wrapper');
                $pdf->loadView('pdf', compact('data'));
                $pdf = $pdf->stream();

        //return PDF::loadFile(public_path().'/myfile.html')->save('/path-to/my_stored_file.pdf')->stream('download.pdf');

               // Storage::disk('local')->put('gmailas.pdf', $pdf->stream());

                $username = User::find(1);
                $article = Article::find(14);
                $email = User::find(11);

               // Mail::to($email->email)->send(new pdfemail($pdf, $username, $article));

//
//               $redis = app()->make('redis');
//               $redis->set("key1", $username);
//
//                return $redis->get("key1");
//

        $start = microtime(true);

        //$result = User::all();

        $result = Cache::remember('pdf', 10, function () use($pdf)  {

            return $pdf;

        });

        $duration = (microtime(true) - $start) * 1000;



        return $result;






        //return "sent";






                //return $pdf;
                

        }


   


    public function googletoken(Article $article){

        $http = new Client;

        $code = '4/AADUzrYsiHY2tDsjwdrKuL-l7TPbHK2BFRyztSmUkVaB8OqASoxfyk-YdyNQrRSSlQXE1RjRFTM8Rrs4moHChow';
        $code = urldecode($code);

        

        $response = $http->post('https://www.googleapis.com/oauth2/v4/token',  [

        'form_params' => [
            'grant_type' => 'authorization_code',
            'code' => $code,
            'client_id' => '183346254246-16qlo8hpj9c4epdrbe7gblsvnr7sjcbe.apps.googleusercontent.com',
            'client_secret' => 'XlXIEbv0xNuUrdtblDghpZPF',
            'redirect_uri' => 'https://accounts.google.com/o/oauth2/v2/auth?',      
        ],

      

    ]); 
    
   // $tok = json_decode($response, true);
   //$tok = $response;
        $contents = json_decode((string) $response->getBody(), true);
        $token = $contents['access_token'];

        Storage::disk('local')->put('token.txt', $token);


       return redirect('api/email');

}





    public function show(Article $article )
    {
        // $this->authorize('view', $article);
        // return $article;
       

        // list buckets example
        // https://accounts.google.com/o/oauth2/v2/auth?
        //     scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive.metadata.readonly&
        //     access_type=offline&
        //     include_granted_scopes=true&
        //     state=state_parameter_passthrough_value&
        //     redirect_uri=http%3A%2F%2Foauth2.example.com%2Fcallback&
        //     response_type=code&
        //     client_id=client_id
        
        
        //$http = new Client;

        // $response = $http->post('https://accounts.google.com/o/oauth2/v2/auth?', [

        //     'form_params' => [
        //         'scope' => 'https://mail.google.com/',
        //         'access_type' => 'offline',
        //         'include_granted_scopes' => 'true',
        //         'state' => 'state_parameter_passthrough_value',
        //         'redirect_uri' => 'http://evolvo.lt',
        //         'response_type' => 'code',
        //         'client_id' => '183346254246-16qlo8hpj9c4epdrbe7gblsvnr7sjcbe.apps.googleusercontent.com',
                
        //     ],
        // ]);

        // $code =  $response;
        // $code = '4%2FAADnS-X5QKOZjs2L6dkFREdymtyTN659KzLg8Dh88TVC3imnCnhRlRzc_YpG7Ab-77GJTW5dqy9yhW2SDQsaZa0';
        //     $code = urldecode($code);

        // $response = $http->post('https://accounts.google.com/o/oauth2/token', [

        //     'form_params' => [
        //         'grant_type' => 'authorization_code',
        //         'code' => $code,
        //         'client_id' => '183346254246-16qlo8hpj9c4epdrbe7gblsvnr7sjcbe.apps.googleusercontent.com',
        //         'client_secret' => 'XlXIEbv0xNuUrdtblDghpZPF',
        //         'redirect_uri' => 'http://evolvo.lt',
    
                
        //     ],
        // ]); 
        
       //return json_decode($response);

        //   $client = new CLient([

        //     'headers' => ['Authorization' =>'Bearer '. $accessToken],
        
        //   ]);

        //   $response = $client->request('GET', 'https://www.googleapis.com/gmail/v1/users/vytautas.sabutis%40gmail.com/messages/16287baae6cfa34d');

        //         $data = $response->getBody();
        //         //$data = json_decode($data);
        //        return $data;
    
      
    //});
   
          
                    

            // Use $middleware->getAccessToken(); and $middleware->getRefreshToken() to get tokens
            // that can be persisted for subsequent requests.




// Send an asynchronous request.
// $request = new \GuzzleHttp\Psr7\Request('GET', 'http://httpbin.org');
// $promise = $client->sendAsync($request)->then(function ($response) {
//     echo 'I completed! ' . $response->getBody();
// });
// $promise->wait();
    }











    public function store(StoreArticle $request)
    {
        $article = Article::create($request->all());

        return response()->json($article, 201);
    }

    public function update(Request $request, Article $article)
    {
       
        $article->update($request->all());

        return response()->json($article, 200);
    }

    public function delete(Article $article)
    {
        $article->delete();

        return response()->json(null, 204);
    }

    public function cache(){

        $start = microtime(true);

        //$result = User::all();

        $result = Cache::remember('users', 10, function () {

            return User::all();

        });

        $duration = (microtime(true) - $start) * 1000;



        //return $result. ' trukme: ' . $duration . ' ms';

        //return Cache::get('users');


       // Storage::disk('local')->put('failas.txt', 'Contents');
        //Storage::disk('s3')->put('file.txt', 'contents');

        Storage::disk('s3')->put('failas.txt', 'adsdadas');

        Storage::disk('s3')->setVisibility('failas.txt', 'private');

       // Storage::disk('s3')->delete('failas.txt');
        $url = Storage::disk('s3')->temporaryUrl(
            'failas.txt', now()->addMinutes(1)
        );



        return $url;
        //return Storage::disk('s3')->get('failas.txt');
    }


}
