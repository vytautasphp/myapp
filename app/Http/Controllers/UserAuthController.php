<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class UserAuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */

    public function username(Request $request){

        $client = new Client;
        $response = $client->request('GET', 'http://myapp.localhost/api/user', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',

                'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjliOTZjNTdlMTYwNmU2M2Y2ZDk5MzI0M2U4OGVlOGE2NjcxNDFmYjRjNWE3N2ZiNjU3YTU0NTk0MjY2YjQxZDliYjY3YWJiMTcyOTRhZGY4In0.eyJhdWQiOiIyIiwianRpIjoiOWI5NmM1N2UxNjA2ZTYzZjZkOTkzMjQzZTg4ZWU4YTY2NzE0MWZiNGM1YTc3ZmI2NTdhNTQ1OTQyNjZiNDFkOWJiNjdhYmIxNzI5NGFkZjgiLCJpYXQiOjE1MjM0NDMxOTYsIm5iZiI6MTUyMzQ0MzE5NiwiZXhwIjoxNTU0OTc5MTk2LCJzdWIiOiIxIiwic2NvcGVzIjpbIioiXX0.VJD-VIuDZnJ3GWdpSs0kJw-lXgfBJyEQXHIiVjg05Z90Y85hwltfBRqXv64a7DAidO2bYeFiR1DKycYKMgJgkmUkoENiRzhSH54JooVHyWAWkDWXzRob2QlZ01_y1s4mrmXZ3e7gyCDAYY4HmV3K_cGrwSJwOQZH1rbR70Q_j2BHejPxJrIssa7mHlTudLH8R2mEcS4gwmyzGnI3_YqHiZ95Y6XIAXPXMFt2bdYDBGSoyFbDP9qrkfAvplnqc6rndZ1jElm__iodAJqsA3o-qzYtR_94oUztPMMhx9Whc6_hG3717HTaaFkRuX16PvrfvI3vcfXy_fefYYgdUcWaZ0-T1yHG8O1hQ_wovI51s7UxvyisRRoTB0ppkK-YG8eXtPGk1qae-9W1tZxfNF--8s2NnwCpgbOkWELKnr0uk_L4Jk_5TM2j1sWxR21VGt2ZAULlM6ODMjKE6NkxhVRqlKUZ8JlydEXC4eEK5qPXVHsGOhSXPiN4GdsZ0MdjLY8smJBmGv56iCVDbHnzxOhVUisMTo7OeZ4Ubhm9LfmXHQ6J5kYre57qt8xbuHaNNHZUZj0bw5EkDVxcG4vUVgGFZ1QcHkw9wzwGTvyxgMh1wqN4DEs7HpeszdzvD8NKwsHcEe5WMl9dF1jA1qVGYOeocoPiiRAJmx0OduEqK0MQd30',
            ],
        ]);
        return $response;


    }


    public function index(Request $request)
    {
//
        return $request->user();
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
