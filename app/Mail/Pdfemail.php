<?php

namespace App\Mail;


use App\Article;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;

class Pdfemail extends Mailable
{
    use Queueable, SerializesModels;


    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $pdf;
    protected $username;
    protected $article;

    public function __construct()
    {
//        $this->pdf = $pdf;
//        $this->username = $username['name'];
//        $this->article = $article->body;

    }



    /**
     * Build the message.
     *
     * @return $this
     */

    public function build()
    {



//        return $this->from('vytautas@example.com','laravel')
//            ->subject($this->username)
//            ->view('msg')->with([
//                'username' => $this->username,
//                'article'  => $this->article,
//            ])
//            ->attachData($this->pdf, 'laravel.pdf');





        return $this->from('vytautas@example.com','laravel')->view('msg')->attach(storage_path('app/gmailas.pdf'));

//        Mail::send('emails.html.for_external', ["client_name"=>$retail_order->client_name], function ($message) use ($retail_order, $pdf) {
//            $message->to($retail_order->client_email);
//            $message->from($retail_order->created_by_name);
//            $message->subject('Barborr komercinis pasiūlymas');
//            $message->attachData($pdf->output(), 'pasiulymas.pdf');
//
//        });


    }


}
