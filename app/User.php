<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    
    use HasApiTokens, Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function withCount($string)
    {
    }

    public function generateToken()
    {
        $this->api_token = str_random(60);
        $this->save();

        return $this->api_token;
    }
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function articles()
    {
        return $this->hasMany('App\Article');
    }

    public function roles()
    {
       // return $this->belongsToMany('App\Role', 'role_user','user_id', 'role_id')->wherePivot('user_id', 1);
        return $this->belongsToMany('App\Role')->wherePivotIn('role_id', [2]);
    }

}
