<?php

use App\Article;
use App\Events\MailSent;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

 


Route::post('register', 'Auth\RegisterController@register');
Route::get('articles/{article}', 'ArticleController@show');
Route::get('google', 'ArticleController@googlecode');
Route::get('googletoken', 'ArticleController@googletoken');
Route::get('email', 'ArticleController@email');
Route::get('username', 'UserAuthController@username');
Route::get('send', 'ArticleController@ship');
Route::get('cache', 'ArticleController@cache');
Route::get('/fireEvent', function () {
    event(new MailSent());
});

Route::get('/alertBox', function () {
    return view('eventListener');
});


Route::get('users/{id}', 'TestController@show');
Route::get('user/comments', 'TestController@showComments');
Route::post('user/store', 'TestController@store');

Route::post('articles', 'ArticleController@store');

Route::get('/admin','TestController@admin');



Route::get('/foo', function () {
    $exitCode = Artisan::call('emailas:command', [
        'firstName' => 'Vytautas', 'lastName' => 'Sabutis']);

    return $exitCode;

    //
});

Route::get('articles', 'ArticleController@index');


 Route::middleware('auth:api')->group(function(){


     Route::get('user', 'UserAuthController@index');

         //dd($request);
         ///return $request->user();

     Route::put('articles/{article}', 'ArticleController@update');
     Route::delete('articles/{article}', 'ArticleController@delete');



 });


Route::get('users', 'TestController@index');
//Jxp2BiYKvIcfJQiwXY6QmRv7YRNl9BXRXR4ZmeX5LCgcltfdSOXF4YRt6tac

