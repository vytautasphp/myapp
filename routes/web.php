<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Events\eventTrigger;
use App\Events\HelloPusherEvent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::post('send', 'ArticleController@ship');
//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/alertBox', function () {
    return view('eventListener');
});

Route::get('/fireEvent', function () {
    event(new eventTrigger());
});

Route::get('/pusher', function() {
    event(new HelloPusherEvent('Hi there Pusher!'));
    return "Event has been sent!";
});

Route::middleware('auth:api')->group(function() {
//    Route::get('/', function () {
//
//        $redis = app()->make('redis');
//        $redis->set("key1", "testvalue");
//
//        return $redis->get("key1");
//
//
//    });

    Route::get('/', 'HomeController@show');
});

Route::get('/home', 'HomeController@index')->name('home');



Route::middleware('cors:api')->group(function() {


    Route::get('/api/hello', function () {

        return "hello there";
    });

});
Auth::routes();
