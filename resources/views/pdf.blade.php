
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

</head>
<body>

<div class="container">
    <h2>Email</h2>

    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Subject</th>
            <th>Message</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$data['id']}}</td>
            <td>{{$data['payload']['headers'][5]['value']}}</td>
            <td>{{$data['snippet']}}</td>
        </tr>

        </tbody>
    </table>
</div>

</body>
</html>